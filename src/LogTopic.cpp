#include "zGothicAPI.h"
#include "LogTopic.h"
#include <sqrat.h>

void LogTopic::Bind()
{
	using namespace SqModule;

	/* squirreldoc (class)
	*
	* This class represents log topic.
	*
	* @side		client
	* @name		LogTopic
	*
	*/
	Sqrat::RootTable().Bind("LogTopic", Sqrat::Class<LogTopic>(vm, "LogTopic")

		/* squirreldoc (property)
		*
		* Represents topic section.
		*
		* @side		client
		* @name     section
		* @return   (int)
		*
		*/
		.SquirrelProp("section", &LogTopic::getSection, &LogTopic::setSection)

		/* squirreldoc (property)
		*
		* Represents topic status.
		*
		* @side		client
		* @name     status
		* @return   (int)
		*
		*/
		.SquirrelProp("status", &LogTopic::getStatus, &LogTopic::setStatus)

		/* squirreldoc (property)
		*
		* Represents topic description.
		*
		* @side		client
		* @name     description
		* @return   (string)
		*
		*/
		.SquirrelProp("description", &LogTopic::getDescription, &LogTopic::setDescription)

		/* squirreldoc (method)
		*
		* This method is used to to add topic entry.
		*
		* @side		client
		* @name		addEntry
		* @param	(string) description of the entry.
		*
		*/
		.Func("addEntry", &LogTopic::addEntry)

		/* squirreldoc (method)
		*
		* This method is used to get list of topic entries.
		*
		* @side		client
		* @name		getEntries
		* @return	(array) an array of entries descriptions.
		*
		*/
		.Func("getEntries", &LogTopic::getEntries)
	);
}

LogTopic::LogTopic()
{
	cLogTopic = new oCLogTopic();
	cLogTopic->m_enuSection = oCLogTopic::zELogSection_Missions;
	cLogTopic->m_enuStatus = oCLogTopic::zELogTopicStatus_Free;
	cLogTopic->m_strDescription = "";
}

LogTopic::~LogTopic()
{
	if (cLogTopic != nullptr) {
		delete cLogTopic;
	}
}

SQInteger LogTopic::getSection(HSQUIRRELVM vm)
{
	LogTopic* self = Sqrat::ClassType<LogTopic>::GetInstance(vm, 1);

	Sqrat::PushVar(vm, self->cLogTopic->m_enuSection);

	return 1;
}

SQInteger LogTopic::setSection(HSQUIRRELVM vm)
{
	LogTopic* self = Sqrat::ClassType<LogTopic>::GetInstance(vm, 1);

	SQInteger section;

	sq_getinteger(vm, 2, &section);

	self->cLogTopic->m_enuSection = (oCLogTopic::zELogSection) section;

	return 1;
}

SQInteger LogTopic::getStatus(HSQUIRRELVM vm)
{
	LogTopic* self = Sqrat::ClassType<LogTopic>::GetInstance(vm, 1);

	Sqrat::PushVar(vm, self->cLogTopic->m_enuStatus);

	return 1;
}

SQInteger LogTopic::setStatus(HSQUIRRELVM vm)
{
	LogTopic* self = Sqrat::ClassType<LogTopic>::GetInstance(vm, 1);

	SQInteger status;

	sq_getinteger(vm, 2, &status);

	self->cLogTopic->m_enuStatus = (oCLogTopic::zELogTopicStatus) status;

	return 1;
}

SQInteger LogTopic::getDescription(HSQUIRRELVM vm)
{
	LogTopic* self = Sqrat::ClassType<LogTopic>::GetInstance(vm, 1);

	Sqrat::string description = self->cLogTopic->m_strDescription;

	Sqrat::PushVar(vm, description);

	return 1;
}

SQInteger LogTopic::setDescription(HSQUIRRELVM vm)
{
	LogTopic* self = Sqrat::ClassType<LogTopic>::GetInstance(vm, 1);

	const SQChar* description = "";

	sq_getstring(vm, 2, &description);

	if (strlen(description) == 0)
		return sq_throwerror(vm, "(LogTopic::setDescription) description string is empty");

	self->cLogTopic->m_strDescription = new zSTRING(description);

	return 1;
}

void LogTopic::addEntry(Sqrat::string entry)
{
	if (entry.length() == 0) {
		SqModule::Error("(LogTopic::addEntry) entry string is empty");
		return;
	}

	this->cLogTopic->AddEntry(entry.c_str());
}

Sqrat::Array LogTopic::getEntries()
{
	Sqrat::Array result;

	auto entries = this->cLogTopic->m_lstEntries;

	for (int i = 0, len = entries.GetNum(); i < len; ++i)
	{
		Sqrat::string str = entries[i]->m_strDescription;

		result.Append(str);
	}

	return result;
}
