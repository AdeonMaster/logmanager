#include "zGothicAPI.h"
#include "LogManager.h"
#include "LogTopic.h"
#include <sqrat.h>

void LogManager::Bind()
{
	using namespace SqModule;

	/* squirreldoc (class)
	*
	* This class represents log manager.
	*
	* @side		client
	* @name		LogManager
	*
	*/
	Sqrat::RootTable().Bind("LogManager", Sqrat::Class<LogManager, Sqrat::NoConstructor<LogManager>>(vm, "LogManager")

		/* squirreldoc (method)
		*
		* This method is used to add new topic.
		*
		* @side		client
		* @name		addTopic
		* @param	(LogTopic) topic to be added.
		*
		*/
		.SquirrelFunc("addTopic", &LogManager::addTopic, 2, "..")

		/* squirreldoc (method)
		*
		* This method is used to get list of topics.
		*
		* @side		client
		* @name		getTopics
		* @return	(array) an array of topics.
		*
		*/
		.SquirrelFunc("getTopics", &LogManager::getTopics, 1, ".")

		/* squirreldoc (method)
		*
		* This method is used to clear all topics.
		*
		* @side		client
		* @name		clear
		*
		*/
		.StaticFunc("clear", &LogManager::clear)
	);
}

SQInteger LogManager::addTopic(HSQUIRRELVM vm)
{
	LogTopic* topic = Sqrat::ClassType<LogTopic>::GetInstance(vm, 2);
	if (topic == nullptr)
		return sq_throwerror(vm, "(LogManager::addTopic) invalid type, expected LogTopic");

	oCLogManager::GetLogManager().m_lstTopics.Insert(topic->cLogTopic);

	return 1;
}

SQInteger LogManager::getTopics(HSQUIRRELVM vm)
{
	Sqrat::Array result(vm);

	for (int i = 0, len = oCLogManager::GetLogManager().m_lstTopics.GetNum(); i < len; ++i)
	{
		oCLogTopic* cLogTopic = oCLogManager::GetLogManager().m_lstTopics[i];

		LogTopic* topic = new LogTopic();
		topic->cLogTopic = cLogTopic;

		result.Append(topic);
	}

	Sqrat::PushVar(vm, result);

	return 1;
}

void LogManager::clear()
{
	oCLogManager::GetLogManager().Clear();
}
