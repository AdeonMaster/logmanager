#include "zGothicAPI.h"
#include <sqrat.h>

class LogManager
{
public:
	static void Bind();

	static SQInteger addTopic(HSQUIRRELVM vm);
	static SQInteger getTopics(HSQUIRRELVM vm);
	static void clear();
};
