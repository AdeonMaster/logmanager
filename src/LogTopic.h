#include <sqrat.h>
#include "zGothicAPI.h"

class LogTopic
{
public:
	oCLogTopic* cLogTopic;

	static void Bind();

	LogTopic();
	~LogTopic();

	static SQInteger getSection(HSQUIRRELVM vm);
	static SQInteger setSection(HSQUIRRELVM vm);
	static SQInteger getStatus(HSQUIRRELVM vm);
	static SQInteger setStatus(HSQUIRRELVM vm);
	static SQInteger getDescription(HSQUIRRELVM vm);
	static SQInteger setDescription(HSQUIRRELVM vm);

	void addEntry(Sqrat::string entry);
	Sqrat::Array getEntries();
};
