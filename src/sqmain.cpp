#include "zGothicAPI.h"
#include "LogTopic.h"
#include "LogManager.h"
#include <sqrat.h>

extern "C" SQRESULT SQRAT_API sqmodule_load(HSQUIRRELVM vm, HSQAPI api)
{
	SqModule::Initialize(vm, api);
	Sqrat::DefaultVM::Set(vm);

	LogTopic::Bind();
	LogManager::Bind();

	Sqrat::ConstTable consttable(vm);

	/* squirreldoc (const)
	*
	* Represents missions section.
	*
	* @category Log sections
	* @side		client
	* @name		LOG_SECTION_MISSIONS
	*
	*/
	consttable.Const("LOG_SECTION_MISSIONS", oCLogTopic::zELogSection_Missions);

	/* squirreldoc (const)
	*
	* Represents notes section.
	*
	* @category Log sections
	* @side		client
	* @name		LOG_SECTION_NOTES
	*
	*/
	consttable.Const("LOG_SECTION_NOTES", oCLogTopic::zELogSection_Notes);

	/* squirreldoc (const)
	*
	* Represents all sections.
	*
	* @category Log sections
	* @side		client
	* @name		LOG_SECTION_ALL
	*
	*/
	consttable.Const("LOG_SECTION_ALL", oCLogTopic::zELogSection_All);

	/* squirreldoc (const)
	*
	* Represents free log topic status.
	*
	* @category Log topic statues
	* @side		client
	* @name		LOG_TOPIC_STATUS_FREE
	*
	*/
	consttable.Const("LOG_TOPIC_STATUS_FREE", oCLogTopic::zELogTopicStatus_Free);

	/* squirreldoc (const)
	*
	* Represents running log topic status.
	*
	* @category Log topic statues
	* @side		client
	* @name		LOG_TOPIC_STATUS_RUNNING
	*
	*/
	consttable.Const("LOG_TOPIC_STATUS_RUNNING", oCLogTopic::zELogTopicStatus_Running);

	/* squirreldoc (const)
	*
	* Represents success log topic status.
	*
	* @category Log topic statues
	* @side		client
	* @name		LOG_TOPIC_STATUS_SUCCESS
	*
	*/
	consttable.Const("LOG_TOPIC_STATUS_SUCCESS", oCLogTopic::zELogTopicStatus_Success);

	/* squirreldoc (const)
	*
	* Represents failure log topic status.
	*
	* @category Log topic statues
	* @side		client
	* @name		LOG_TOPIC_STATUS_FAILURE
	*
	*/
	consttable.Const("LOG_TOPIC_STATUS_FAILURE", oCLogTopic::zELogTopicStatus_Failure);

	/* squirreldoc (const)
	*
	* Represents absolete log topic status.
	*
	* @category Log topic statues
	* @side		client
	* @name		LOG_TOPIC_STATUS_OBSOLETE
	*
	*/
	consttable.Const("LOG_TOPIC_STATUS_OBSOLETE", oCLogTopic::zELogTopicStatus_Obsolete);

	return SQ_OK;
}
